from subtitlesApi import subtitles
import connector
import interfaces


class OpenSubtitles(subtitles.Subtitles):
    def __init__(self):
        self.get_subtitles_interface = interfaces.GetOpenSubtitlesInterface()
        self.search_subtitles_interface = interfaces.SearchOpenSubtitlesByMovie()
        self.open_subtitles_connector = connector.OpenSubtitlesConnector()

    def set_search_interface(self, search_interface):
        self.search_subtitles_interface = search_interface

    def set_get_interface(self, get_interface):
        self.get_subtitles_interface = get_interface

    def get_subtitles(self, *args, **kwargs):
        return self.get_subtitles_interface.get(self.open_subtitles_connector, kwargs['file_id'], *args, **kwargs)

    def search_subtitles(self, *args, **kwargs):
        return self.search_subtitles_interface.search(self.open_subtitles_connector, kwargs['file_path'], *args, **kwargs)


class OpenSubtitlesData(subtitles.SubtitlesData):

    def __init__(self, status, seconds, data=''):
        self.status = status
        self.seconds = seconds
        self.data = data

    def __repr__(self):
        return '<SubtitlesData {0}>'.format(self.status)

    def __str__(self):
        return '<SubtitlesData {0}>'.format(self.status)

    def return_subtitles(self):
        return [OpenSubtitle(**sub) for sub in self.data]


class OpenSubtitle(object):
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __str__(self):
        return '<Subtitle {0}>'.format(getattr(self, 'SubFileName', None))