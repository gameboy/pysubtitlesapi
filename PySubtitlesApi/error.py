class StatusError(Exception):
    pass


class NoSubtitlesError(Exception):
    pass


class FileNotExistError(OSError):
    pass


class GetSubtitlesError(Exception):
    pass


class SearchSubtitlesError(Exception):
    pass
