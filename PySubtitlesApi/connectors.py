import abc


class SubtitlesConnector(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def connect(self):
        pass
