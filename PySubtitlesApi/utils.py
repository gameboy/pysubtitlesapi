import struct
import os
import error


def hashFile(name):
        longlongformat = 'q'  # long long
        bytesize = struct.calcsize(longlongformat)

        f = open(name, "rb")

        filesize = os.path.getsize(name)
        hash = filesize

        if filesize < 65536 * 2:
            return "SizeError"

        for x in range(int(65536/bytesize)):
            buffer = f.read(bytesize)
            (l_value,) = struct.unpack(longlongformat, buffer)
            hash += l_value
            hash = hash & 0xFFFFFFFFFFFFFFFF #to remain as 64bit number

        f.seek(max(0, filesize-65536), 0)
        for x in range(int(65536/bytesize)):
            buffer = f.read(bytesize)
            (l_value,) = struct.unpack(longlongformat, buffer)
            hash += l_value
            hash = hash & 0xFFFFFFFFFFFFFFFF

        f.close()
        returnedhash = "%016x" % hash
        return returnedhash


class FileProperties(object):

    @staticmethod
    def get_file_size(file):
        try:
            file_size = str(os.path.getsize(file))
            return file_size
        except OSError:
            raise error.FileNotExistError("No such file {0}".format(file))

    @staticmethod
    def get_file_hash(file):
        try:
            file_hash = str(hashFile(file))
            return file_hash
        except IOError:
            raise error.FileNotExistError("No such file {0}".format(file))
