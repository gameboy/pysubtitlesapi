class GetSubtitlesInterface(object):
    def get(self):
        raise NotImplementedError


class SearchSubtitlesInterface(object):
    def search(self):
        raise NotImplementedError