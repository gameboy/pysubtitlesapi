import abc


class Subtitles(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_subtitles(self):
        pass

    @abc.abstractmethod
    def search_subtitles(self):
        pass


class SubtitlesData(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def return_subtitles(self):
        pass

