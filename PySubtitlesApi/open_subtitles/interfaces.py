import base64

from subtitlesApi import interfaces
from subtitlesApi import utils
from subtitlesApi import error

import connector as open_subtitles_connector
import subtitles as open_subtitles_subtitles


class GetOpenSubtitlesInterface(interfaces.GetSubtitlesInterface):
    def get(self, connector, *args, **kwargs):
        file_id = kwargs['file_id']
        subtitles_raw_data = connector.proxy_server.DownloadSubtitles(connector.get_token(),
                                                                      [file_id])
        if not subtitles_raw_data['status'] == open_subtitles_connector.OpenSubtitlesConnector.RETURNED_OK_STATUS:
            raise Exception()
        data = subtitles_raw_data['data']
        print data
        # for d in data:
        #     print d
        # data = base64.b64decode(data)
        # f = open('/home/gameboy/adfaf.zip', 'w')
        # f.write(data)
        # f.close()


class SearchOpenSubtitlesByMovie(interfaces.SearchSubtitlesInterface):
    def search(self, connector, *args, **kwargs):
        file_path = kwargs['file_path']
        hash_file = utils.FileProperties.get_file_hash(file_path)
        size_file = utils.FileProperties.get_file_size(file_path)
        subtitles_raw_data = connector.proxy_server.SearchSubtitles(connector.get_token(),
                                                                    [{'moviebytesize': size_file,
                                                                      'moviehash': hash_file}]
        )
        if not subtitles_raw_data['status'] == open_subtitles_connector.OpenSubtitlesConnector.RETURNED_OK_STATUS:
            raise error.SearchSubtitlesError(subtitles_raw_data['status'])
        sb = open_subtitles_subtitles.OpenSubtitlesData(subtitles_raw_data['status'],
                                                        subtitles_raw_data['seconds'],
                                                        subtitles_raw_data['data'])
        return sb