from subtitlesApi.open_subtitles import subtitles as open_subtitles


class ProviderNotImplemented(Exception):
    pass


class SubtitlesFactory(object):
    subtitles_type = {'open_subtitles': open_subtitles.OpenSubtitles()}

    @staticmethod
    def factory(type):
        class_ = SubtitlesFactory.subtitles_type.get(type)
        if class_:
            return class_
        raise ProviderNotImplemented()
