import six
from subtitlesApi import error

if six.PY3:
    from xmlrpc.client import ServerProxy
else:
    from xmlrpclib import ServerProxy


class OpenSubtitlesConnector(object):
    RETURNED_OK_STATUS = '200 OK'
    API_ADDRESS = 'http://api.opensubtitles.org/xml-rpc'

    def __init__(self, user_name='', password='', language='', agent='OS Test User Agent'):
        self.username = user_name
        self.password = password
        self.language = language
        self.agent = agent
        self.proxy_server = ServerProxy(OpenSubtitlesConnector.API_ADDRESS)

    def get_token(self):
        if hasattr(self, 'token'):
            return self.token
        return self.__get_token()

    def __get_token(self):
        data = self.proxy_server.LogIn(self.username, self.password, self.language, self.agent)
        status = data['status']
        if not status == OpenSubtitlesConnector.RETURNED_OK_STATUS:
            raise error.StatusError(status)
        return data['token']

    def __del__(self):
        if hasattr(self, 'token'):
            self.proxy_server.LogOut(self.token)